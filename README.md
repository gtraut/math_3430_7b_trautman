# U.S. States
Created by Gabe Trautman for BGSU MATH 3430
### Files
* **gtraut_States.csv** - full dataset
* **gtraut_States_E.csv** - dataset including 4 distinct errors according to the assignment
### Attributes
* **Name** - full name of the state
* **Region** - region in which the state is located according to [Wikipedia](https://en.wikipedia.org/wiki/List_of_regions_of_the_United_States))
* **Happiness** - happiness index according to a study by [WalletHub](https://wallethub.com/edu/happiest-states/6959/)
* **SnS** - number of Steak 'n' Shake locations in the state according to [Loc8NearMe](https://www.loc8nearme.com/steak-n-shake/)